# CRUX ports
Name | Version | Description
-|-|-
[libdockapp](https://www.dockapps.net/) | 0.7.3 | DockApp Development Standard Library
[pinentry](https://gnupg.org/related_software/pinentry/) | 1.1.1 | Secure PinEntry Dialog
[pkgmk.conf](https://github.com/therealfun/crux-ports/tree/master/pkgmk.conf) | 1.84 | Plugins for pkgmk
[spice-gtk](https://www.spice-space.org/) | 0.39 | GTK viewer for the SPICE clients
[spice-protocol](https://www.spice-space.org/) | 0.14.3 | Headers defining SPICE protocols
[spice-server](https://www.spice-space.org/) | 0.14.3 | SPICE library (server)
[spiped](https://www.tarsnap.com/spiped.html) | 1.6.2 | A utility for creating symmetrically encrypted and authenticated pipes
[tigervnc](https://tigervnc.org/) | 1.12.0 | High performance, multi-platform VNC client and server
[tinyssh](https://tinyssh.org/) | 20220305 | Minimalistic SSH server
[umkinitramfs](https://gitlab.com/therealfun/crux-ports/tree/master/umkinitramfs) | 0.7.8 | Simple initramfs generator
[upkg](https://github.com/therealfun/crux-ports/upkg) | 0.94 | Smaller/fancier prt-get/pkgmk/ports replacements
[windowmaker-dockapps](https://www.dockapps.net/) | 20210906 | Window Maker dockapps
[windowmaker](https://windowmaker.org) | 0.95.9 | Fast, feature rich, and easy to use window manager
